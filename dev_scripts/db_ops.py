import sqlite3
import pandas as pd
import numpy as np

class db_ops:
    def __init__(self):
        self.db_name = "db/intervar.db"

    def connect(self): # Returns a cursor object
        conn = sqlite3.connect(self.db_name)  # You can create a new database by changing the name within the quotations
        c = conn.cursor()
        return c, conn

    def query_variants(self, sample_id):
        c, conn = self.connect()
        df = pd.read_sql_query('SELECT * FROM variants2patients as v2p \
          LEFT JOIN annotated_variants AS av ON v2p.CHROM=av.CHROM AND v2p.POS = av.POS AND v2p.REF = av.REF AND v2p.ALT = av.ALT \
          LEFT JOIN interpretations AS i ON v2p.ID=i.varID \
          WHERE sample_ID = sample_id', conn) # Ny left join her:
        return df

    def return_html(self, df, tab_id): # returns a html from a dataframe
        return df.to_html(table_id = tab_id, index_names = False, index=False) # .iloc[0:10,0:6]

    def return_filtered_df(self, df):
        ''' Add column "category" and filter dominant, recessive and DMs'''
        f_df = df
        f_df['category'] = np.nan
        df_dm = f_df[(f_df.ExAC_AF_NFE < 0.05) | (f_df.ExAC_AF_NFE.isnull()) ]
        df_dm.loc[:, 'HGMD_class'] = df_dm['HGMD_class'].astype(str)
        df_dm = df_dm[(df_dm['HGMD_class'].str.contains("dm", case=False, na=False)) |(df_dm['PATHOGENIC'] > 0) | (df_dm['LIKELY_PATHOGENIC'] > 0)  ]
        df_dm['category'] = "dm"
        # Recessive:
        df_rec = f_df[ (f_df.ExAC_AF_NFE < 0.05) | (f_df.ExAC_AF_NFE.isnull()) ]
        df_rec = df_rec[ (df_rec.ExAC_AF < 0.05) | (df_rec.ExAC_AF.isnull()) ]
        df_rec = df_rec[ (df_rec.gnomAD_AF_NFE < 0.05) | (df_rec.gnomAD_AF_NFE.isnull()) ]
        df_rec = df_rec[ (df_rec.inhouse_het < 30) | (df_rec.inhouse_het.isnull()) ]
        df_rec = df_rec[ (df_rec.inhouse_hom < 15) | (df_rec.inhouse_hom.isnull()) ]
        df_rec = df_rec[~(df_rec['Annotation'] == "upstream_gene_variant")]
        df_rec = df_rec[~(df_rec['Annotation'] == "intron_variant")]
        df_rec = df_rec[~(df_rec['Annotation'] == "5_prime_UTR_variant")]
        df_rec = df_rec[~(df_rec['Annotation'] == "intragenic_variant")]
        df_rec = df_rec[~(df_rec['Annotation'] == "synonymous_variant")]
        df_rec = df_rec[~(df_rec['Annotation'] == "3_prime_UTR_variant")]
        df_rec = df_rec[~(df_rec['Annotation'] == "downstream_gene_variant")]
        df_rec = df_rec[~(df_rec['Annotation'] == "intergenic_region")]
        # hom eller to varianter i samme gen
        df_rec = df_rec[(df_rec.duplicated('Gene_name', keep=False) == True & ~df_rec.duplicated('POS', keep=False)) | (df_rec.GT.astype(str).str.contains("1/1")) | (df_rec.GT.astype(str).str.contains("1\|1"))]
        df_rec['category'] = "rec"
        # Dominant:
        df_dom = f_df[(f_df.ExAC_AF_NFE < 0.01) | (f_df.ExAC_AF_NFE.isnull()) ]
        df_dom = df_dom[(df_dom.ExAC_AF < 0.01) | (df_dom.ExAC_AF.isnull()) ]
        df_dom = df_dom[(df_dom.gnomAD_AF_NFE < 0.01) | (df_dom.gnomAD_AF_NFE.isnull()) ]
        df_dom = df_dom[(df_dom.inhouse_het < 20) | (df_dom.inhouse_het.isnull()) ]
        df_dom = df_dom[(df_dom.inhouse_hom < 10) | (df_dom.inhouse_hom.isnull()) ]
        df_dom = df_dom[~(df_dom['Annotation'] == "upstream_gene_variant")]
        df_dom = df_dom[~(df_dom['Annotation'] == "intron_variant")]
        df_dom = df_dom[~(df_dom['Annotation'] == "5_prime_UTR_variant")]
        df_dom = df_dom[~(df_dom['Annotation'] == "intragenic_variant")]
        df_dom = df_dom[~(df_dom['Annotation'] == "synonymous_variant")]
        df_dom = df_dom[~(df_dom['Annotation'] == "3_prime_UTR_variant")]
        df_dom = df_dom[~(df_dom['Annotation'] == "downstream_gene_variant")]
        df_dom = df_dom[~(df_dom['Annotation'] == "intergenic_region")]
        df_dom['category'] = "dom"

        # Reset index
        return  pd.concat([df_dm, df_rec, df_dom], ignore_index = True)

    def insert_interp(self):
        pass
        #c.execute("INSERT INTO interpretations (varID, comment, class, date) VALUES (?, ?, ?, ?)", (1, "This fucking variant is not anything at all", 2,str(datetime.now()).split(' ')[0]))
        #c.execute("INSERT INTO interpretations (varID, comment, class, date) VALUES (?, ?, ?, ?)", (8, "This fucking variant is not anything at all", 2,str(datetime.now()).split(' ')[0]))
        #conn.commit()