from __future__ import division
# -*- coding: utf-8 -*-
# Import statements
import pandas as pd

class intervar:
    def __init__(self, inFile, df = None, html_table = None):
        self.df = df
        self.html_table = html_table
        self.inFile = inFile

    def return_df(self, d = pd.DataFrame()): # Reads a csv-file in and returns a pandas dataframe
        if d.empty:
            self.df = pd.read_csv(self.inFile, sep='\t')
        else:
            self.df = d
        return self

    def return_html(self, tab_id, filtered): # returns a html from a dataframe
        if not filtered:
            return self.df.to_html(table_id = tab_id, index_names = False, index=False) # .iloc[0:10,0:6]
        else:
            return self.filtered.to_html(table_id = tab_id, index_names = False, index = False)

    def getDfInfo(self):
        # Returns a tuple (rows:columns)
        return self.df.shape

    def getCols(self):
        # Get the column headers from the variant file
        return self.df.columns.values

    def getColsListOfTuples(self, Cols):
        # Returns a list of tuples for use in the FilterForm like [(1, 'Chr'),(2, 'Start'), ...]
        return zip(range(1,len(list(Cols))), list(Cols))

    def getColDtype(self, d, col_idx):
        # Returns datatype for column input
        return self.df.dtypes[col_idx]






'''
OBSOLETE
def filterDf(self, col, condOp, cond, keepIfMiss, formColDtype):
     Function to filter a df:
    df: Dataframe to filter on
    col: column index that is target for filtering
    condOp: eq, not eq to, contains, not contains, starts with, not stat with, greater than less than
    ha med or denne hvis "keep if missing" df[df['var2'].isnull()]
    

    # Convert to correct dtypes before comparison:
    # cond = user input, data type is the data in the pandas df:
    # comp = data fra kolonnen som sammenligned mot
    ### Move this bit to a separate function?
    print "--"
    print self.df[col].dtype
    print keepIfMiss
    print "--"
    if self.df[col].dtype == 'object':
        cond = str(cond).lower().strip()
        comp = self.df[col].str.lower()
    elif self.df[col].dtype == 'int64' or self.df[col].dtype == 'float64'  :
        cond = float(cond)
        comp = self.df[col]
    ###
    if condOp == 'eq': # Solve for X & Y
        #self.filtered = self.df # returns the whole frame
        self.filtered = self.df[comp == cond]
        if keepIfMiss == True:
            self.filtered = self.filtered.append(self.df[comp.isna()])
    elif condOp == 'neq':
        self.filtered = self.df[comp != cond]
        if keepIfMiss == True:
            self.filtered = self.filtered.append(self.df[comp.isna()])
    elif condOp == 'contains':
        print "{} {} {} {} {}".format(col , condOp , cond , keepIfMiss, formColDtype)
        self.filtered = self.df[self.df[col].apply(str).str.contains(cond, na=False, case=False)] # Alter dtype to string before search
        if keepIfMiss == True:
            self.filtered = self.filtered.append(self.df[comp.isna()])
    elif condOp == 'ncontains':
        print "{} {} {} {} {}".format(col , condOp , cond , keepIfMiss, formColDtype)
        self.filtered = self.df[~self.df[col].apply(str).str.contains(cond, na=False, case=False)] # Alter dtype to string before search
        if keepIfMiss == True:
            self.filtered = self.filtered.append(self.df[comp.isna()])
    elif condOp == 'startsw':
        print "{} {} {} {} {}".format(col , condOp , cond , keepIfMiss, formColDtype)
        self.filtered = self.df[self.df[col].apply(str).str.contains('^' + str(cond), na=False, case=False)] # Alter dtype to string before search
        if keepIfMiss == True:
            self.filtered = self.filtered.append(self.df[comp.isna()])
    elif condOp == 'nstartsw':
        print "{} {} {} {} {}".format(col , condOp , cond , keepIfMiss, formColDtype)
        self.filtered = self.df[~self.df[col].apply(str).str.contains('^' + str(cond), na=False, case=False)] # Alter dtype to string before search
        if keepIfMiss == True:
            self.filtered = self.filtered.append(self.df[comp.isna()])
    elif condOp == 'greater':
        print "col:{} condOp:{} cond{} keepIfMiss:{} formColDtype:{}".format(col , condOp , cond , keepIfMiss, formColDtype)
        self.filtered = self.df[self.df[col].astype(int) > cond]
        if keepIfMiss == True:
            self.filtered = self.filtered.append(self.df[comp.isna()])
    elif condOp == 'less':
        print "col:{} condOp:{} cond{} keepIfMiss:{} formColDtype:{}".format(col , condOp , cond , keepIfMiss, formColDtype)
        self.filtered = self.df[ self.df[col].astype(float) < cond]
        if keepIfMiss == True:
            self.filtered = self.filtered.append(self.df[comp.isna()])
    return self.filtered
'''
#
