import pandas as pd


def skip_scientific(df):
    ''' Skip scientific notation on the following fields'''
    listOfFields = ['ExAC_AF_NFE', 'gnomAD_AF_NFE']
    for field in listOfFields:
        df[[field]] = df[[field]].apply(pd.to_numeric, args=('coerce', ))
        df[[field]] = df[[field]].round(6)
        df[field] = df[field].fillna(0).astype(float)
    return df


def ad(df):

    ''' Take a pandas df as input and apply autosomal dominant filtering'''

    df = df[ (df.ExAC_AF_NFE < 0.01) | (df.ExAC_AF_NFE.isnull()) ]
    df = df[(df.chr == "1") | (df.chr == "2") ]
    df = df[(df['FILTER'] == "PASS")]
    listofcols = ["upstream_gene_variant", "intron_variant", "intragenic_variant", "synonymous_variant", "3_prime_UTR_variant", "downstream_gene_variant", "intergenic_region"]
    for col in listofcols:
        if df.shape[0] != 0:
            df = df[~(df['ANN'].str.split('|', expand = True)[1] == col)]
            

        else:
            pass



    filtered_df = df
    return filtered_df

'''

####################################################################
    #
    # To get rid of scientific notation:
    #
    ####################################################################
    new_df['gnomAD_AF_NFE'] = new_df['gnomAD_AF_NFE'].apply(pd.to_numeric, args=('coerce',))
    new_df['gnomAD_AF_FIN'] = new_df['gnomAD_AF_FIN'].apply(pd.to_numeric, args=('coerce',))
    new_df['gnomAD_AF_EAS'] = new_df['gnomAD_AF_EAS'].apply(pd.to_numeric, args=('coerce',))
    new_df['gnomAD_AF_OTH'] = new_df['gnomAD_AF_OTH'].apply(pd.to_numeric, args=('coerce',))
    new_df['gnomAD_AF_ASJ'] = new_df['gnomAD_AF_ASJ'].apply(pd.to_numeric, args=('coerce',))
    new_df['gnomAD_AF_AMR'] = new_df['gnomAD_AF_AMR'].apply(pd.to_numeric, args=('coerce',))
    new_df['gnomAD_AF_AFR'] = new_df['gnomAD_AF_AFR'].apply(pd.to_numeric, args=('coerce',))
    new_df['ExAC_AF_NFE'] = new_df['ExAC_AF_NFE'].apply(pd.to_numeric, args=('coerce',))
    new_df['ExAC_AF_FIN'] = new_df['ExAC_AF_FIN'].apply(pd.to_numeric, args=('coerce',))
    new_df['ExAC_AF_EAS'] = new_df['ExAC_AF_EAS'].apply(pd.to_numeric, args=('coerce',))
    new_df['ExAC_AF_AMR'] = new_df['ExAC_AF_AMR'].apply(pd.to_numeric, args=('coerce',))
    new_df['ExAC_AF_AFR'] = new_df['ExAC_AF_AFR'].apply(pd.to_numeric, args=('coerce',))
    #
    new_df['ExAC_AC_NFE'] = new_df['ExAC_AC_NFE'].fillna(0).astype(int)
    new_df['ExAC_Hom_NFE'] = new_df['ExAC_Hom_NFE'].fillna(0).astype(int)
    new_df['gnomAD_AC_NFE'] = new_df['gnomAD_AC_NFE'].fillna(0).astype(int)
    new_df['gnomAD_Hom_NFE'] = new_df['gnomAD_Hom_NFE'].fillna(0).astype(int)
    #
    new_df['ExAC_AF'] = new_df['ExAC_AF'].apply(pd.to_numeric, args=('coerce',))
    # Go from float to int on count columns:
    new_df['InHouse_notpass'] = new_df['InHouse_notpass'].fillna(0).astype(int)
    new_df['inhouse_het'] = new_df['inhouse_het'].fillna(0).astype(int)
    new_df['inhouse_hom'] = new_df['inhouse_hom'].fillna(0).astype(int)
    new_df['PATHOGENIC'] = new_df['PATHOGENIC'].fillna(0).astype(int)
    new_df['LIKELY_PATHOGENIC'] = new_df['LIKELY_PATHOGENIC'].fillna(0).astype(int)
    new_df['UNCERTAIN_SIGNIFICANCE'] = new_df['UNCERTAIN_SIGNIFICANCE'].fillna(0).astype(int)
    new_df['LIKELY_BENIGN'] = new_df['LIKELY_BENIGN'].fillna(0).astype(int)
    new_df['BENIGN'] = new_df['BENIGN'].fillna(0).astype(int)







df_dom = new_df[ (new_df.ExAC_AF_NFE < 0.01) | (new_df.ExAC_AF_NFE.isnull()) ]
    df_dom = df_dom[ (df_dom.ExAC_AF < 0.01) | (df_dom.ExAC_AF.isnull()) ]
    df_dom = df_dom[ (df_dom.gnomAD_AF_NFE < 0.01) | (df_dom.gnomAD_AF_NFE.isnull()) ]
    df_dom = df_dom[ (df_dom.inhouse_het < 20) | (df_dom.inhouse_het.isnull()) ]
    df_dom = df_dom[ (df_dom.inhouse_hom < 10) | (df_dom.inhouse_hom.isnull()) ]
    df_dom = df_dom[ (df_dom.ExAC_AC_NFE < 100) | (df_dom.ExAC_AC_NFE.isnull()) ]
    df_dom = df_dom[ (df_dom.ExAC_Hom_NFE < 50) | (df_dom.ExAC_Hom_NFE.isnull()) ]
    df_dom = df_dom[ (df_dom.gnomAD_AC_NFE < 100) | (df_dom.gnomAD_AC_NFE.isnull()) ]
    df_dom = df_dom[ (df_dom.gnomAD_Hom_NFE < 50) | (df_dom.gnomAD_Hom_NFE.isnull()) ]
    df_dom = df_dom[ (df_dom.InHouse_notpass < 100) | (df_dom.InHouse_notpass.isnull()) ]
    #df_dom = df_dom[(~df_dom.Annotation.str.contains("upstream_gene_variant|intron_variant|5_prime_UTR_variant|intragenic_variant|synonymous_variant|3_prime_UTR_variant|downstream_gene_variant|intergenic_region"))]
    df_dom = df_dom[~(df_dom['Annotation'] == "upstream_gene_variant")]
    df_dom = df_dom[~(df_dom['Annotation'] == "intron_variant")]
    df_dom = df_dom[~(df_dom['Annotation'] == "5_prime_UTR_variant")]
    df_dom = df_dom[~(df_dom['Annotation'] == "intragenic_variant")]
    df_dom = df_dom[~(df_dom['Annotation'] == "synonymous_variant")]
    df_dom = df_dom[~(df_dom['Annotation'] == "3_prime_UTR_variant")]
    df_dom = df_dom[~(df_dom['Annotation'] == "downstream_gene_variant")]
    df_dom = df_dom[~(df_dom['Annotation'] == "intergenic_region")]
    df_dom = df_dom[(df_dom['FILTER'] == "PASS")]





'''
