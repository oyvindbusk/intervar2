

// FUnctions

$(document).ready( function () {
  console.log("test");

    // Hent inn varianttabell fra flask:
  var data = {{ variantsDict| tojson | safe  }};
  
  // Hent ut verdi 1 i tabell, og fyll med info
  var value = $("#vartable1 tbody tr").closest('tr').children('td:first').text();
  // Fyll inn form elements fra json-fil:
  function fillFromJSON(json, val) {
    if (jQuery.isEmptyObject(json)) {
    } else {
    $("#variant_comment").text(json[val].comment);
    }
  }
    // Skjul form for å sende json
  //$("#jsdata").hide();
  fillFromJSON(data, value);

  if (jQuery.isEmptyObject(data)) { // Skip if no variants
  } else {
    $("#var_info_h5").text("Gen: " + data[value]["ANN"].split('|')[3] + " cDNA: " + data[value]["ANN"].split('|')[9]);
    $("#var_info_card_text").html("Chr:  <strong>" + data[value]["chr"] + "</strong>  Pos:  <strong>" + data[value]["pos"] + " </strong>  Ref:  <strong>" + data[value]["ref"] + " </strong> Alt:  <strong>" + data[value]["alt_allele"] + "</strong>");
    $("#var_info_card_text_1").html("Mutasjonstype:  <strong>" + data[value]["ANN"].split('|')[1] + "</strong>  Transkript:  <strong>" + data[value]["ANN"].split('|')[6] + " </strong>  Exon:  <strong>" + data[value]["ANN"].split('|')[8] + " </strong>");
    $("#var_info_card_text_2").html("AD: <strong>" + data[value]["AD"]  + "</strong> PL: <strong>" + data[value]["PL"] +  "</strong> DP: <strong>" + data[value]["DPth"]+  "</strong> GQ: <strong>" + data[value]["GQ"] + "</strong> GT: <strong>" + data[value]["GT"] + "</strong> FILTER: <strong>" + data[value]["FILTER"] + "</strong>");
    $("#var_info_card_text_3").html("gnomAD: <strong> </strong> ExAC_AF_NFE: <strong>" + data[value]["ExAC_AF_NFE"] + "</strong> ExAC_AN_NFE: <strong>" + data[value]["ExAC_AN_NFE"] + "</strong> ExAC_AC_NFE: <strong>" +  data[value]["ExAC_AC_NFE"] + "</strong> ExAC_HOM_NFE: <strong>" + data[value]["ExAC_HOM_NFE"] + "</strong> ExAC_AN: <strong>" + data[value]["ExAC_AN"] + "</strong> ExAC_AC:<strong> " + data[value]["ExAC_AC"] + "</strong> ExAC_HOM:<strong> " + data[value]["ExAC_HOM"] + "</strong> ExAC_AF: <strong>" + data[value]["ExAC_AF"] + "</strong>"  );
  
    // Link card:
    $("#link_card_href").attr("href", "https://gnomad.broadinstitute.org/variant/" + data[value]["chr"] + "-" + data[value]["pos"] + "-" + data[value]["ref"] + "-" + data[value]["alt_allele"]  );
    $("#link_card_href_2").attr("href", "https://www.omim.org/search/?index=entry&sort=score+desc%2C+prefix_sort+desc&start=1&limit=10&search=" + data[value]["ANN"].split('|')[3]   );
  }
  // Oppdater deretter hver gang det klikkes:
  $("#vartable1 tbody tr").click(function() {
    value = $(this).closest('tr').children('td:first').text();
    $("#var_info_h5").text("Gen: " + data[value]["ANN"].split('|')[3] + " cDNA: " + data[value]["ANN"].split('|')[9]);
    $("#var_info_card_text").html("Chr:  <strong>" + data[value]["chr"] + "</strong>  Pos:  <strong>" + data[value]["pos"] + " </strong>  Ref:  <strong>" + data[value]["ref"] + " </strong> Alt:  <strong>" + data[value]["alt_allele"] + "</strong>");
    $("#var_info_card_text_1").html("Mutasjonstype:  <strong>" + data[value]["ANN"].split('|')[1] + "</strong>  Transkript:  <strong>" + data[value]["ANN"].split('|')[6] + " </strong>  Exon:  <strong>" + data[value]["ANN"].split('|')[8] + " </strong>");
    $("#var_info_card_text_2").html("AD: <strong>" + data[value]["AD"]  + "</strong> PL: <strong>" + data[value]["PL"] +  "</strong> DP: <strong>" + data[value]["DPth"]+  "</strong> GQ: <strong>" + data[value]["GQ"] + "</strong> GT: <strong>" + data[value]["GT"] + "</strong> FILTER: <strong>" + data[value]["FILTER"] + "</strong>");
    $("#var_info_card_text_3").html("gnomAD: <strong> </strong> ExAC_AF_NFE: <strong>" + data[value]["ExAC_AF_NFE"] + "</strong> ExAC_AN_NFE: <strong>" + data[value]["ExAC_AN_NFE"] + "</strong> ExAC_AC_NFE: <strong>" +  data[value]["ExAC_AC_NFE"] + "</strong> ExAC_HOM_NFE: <strong>" + data[value]["ExAC_HOM_NFE"] + "</strong> ExAC_AN: <strong>" + data[value]["ExAC_AN"] + "</strong> ExAC_AC:<strong> " + data[value]["ExAC_AC"] + "</strong> ExAC_HOM:<strong> " + data[value]["ExAC_HOM"] + "</strong> ExAC_AF: <strong>" + data[value]["ExAC_AF"] + "</strong>"  );
    // Ikke fyll hvis ingen verdi i kommentar:
    if (data[value].comment != null) {
    $("#variant_comment").val(data[value].comment);
    } else { 
      $("#variant_comment").val("")
    }
    $("#sel1").val(data[value].variant_class) ;
    
    
});





  // insert jsdata into form element:
  // But only selected items
  // Iterate the original json object and create a new one containing only the fields that are being sent back to flask
  function insertJsonToForm(json) {
    var returnJson = {}
    $.each(json, function(i, obj) {
        //use obj.id and obj.name here, for example:
        //alert(obj.name);
        // It is only the fill-in fields that need to be sent.. E.g. class, comment. Lets pretend ANN neds to be sent as well, since the rest is empty.
        console.log(obj["iid"])
        returnJson[i] = {comment: obj.comment, variant_class: obj.variant_class, patient_id: "{{ pID }}", interp_id: obj["iid"]}
      });
      console.log("--_");
      console.log(returnJson);
      console.log("_--");
    $("#jsdata").text(JSON.stringify(returnJson))
  }

// Detekterer endring i variant class og oppdaterer json

$('#sel1').bind('input propertychange', function() {
  data = updateJson(data, value);
  insertJsonToForm(data)
});

// Detekterer endring i session comment og setter inn i json
  $('#variant_comment').bind('input propertychange', function() {

        //
        if(this.value.length){
          data = updateJson(data, value);
          insertJsonToForm(data)
          console.log("test");
        }
  });

  insertJsonToForm(data)
  
  function updateJson(json, val) {
    // Get values from form elements, and update json object with these:
    json[val].comment = $("#variant_comment").val();
    json[val].variant_class = $("#sel1").val();
    json[val].session_comments = $("#comment").val();
    return json
  }

  $('#vartable1').DataTable({
    select: 'single',
    paging: false,
    searching: false,
  });

});

/*
Code examples not used:

// Using a button to show / hide a card
$('#fieldcard').toggle('show');
  // Show hide card with col fields:
  $("#hidefieldbutton").click(function() {
    $('#fieldcard').toggle('show');
  });




*/