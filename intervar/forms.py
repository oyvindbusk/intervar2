from flask_wtf import FlaskForm
from wtforms import TextField, TextAreaField, SubmitField, FileField, SelectField, HiddenField, validators, BooleanField, FieldList, FormField, StringField


################################################################################################################################################
# Form classes
################################################################################################################################################

class Interpretation(FlaskForm):
    '''
    Form used on the interpretation session site to send POST wiht pID

    '''
    pID = HiddenField("pID")
    jsdata =TextAreaField("javascript_data")
    comments = TextAreaField("Comments")
    submit = SubmitField("Save interpretation")

class Interpretations_form(FlaskForm):
    '''
    Form used on the interpretation session site to send POST wiht pID

    '''
    pID = HiddenField("pID")
    submit = SubmitField("Go to interpretation")
