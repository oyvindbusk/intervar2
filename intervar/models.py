from intervar import db
from flask_login import UserMixin
from sqlalchemy import Integer, ForeignKey, CheckConstraint, DateTime

import datetime

'''
Relationships:
Legend:
>->> one to many
>>->> many to many

Finished:
User                    >->>        Interpretation_session
Patients                >->>        Interpretation_session
Patients                >->>        Samples
Runs                    >->>        Samples
Variants                >->>        Interpretations
Variants                >->>        Annotations

Todo:


Variants                >->>        Annotations
Variants                >->>        Interpretations
Interpretation_session  >->>        Interpretations
Samples                 >>->>       Variants

'''

class User(db.Model, UserMixin):

    __tablename__ = "user"

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String, nullable=False)
    password = db.Column(db.String, nullable=False)
    interpretations = db.relationship('Interpretation_session', backref='user')


    def __repr__(self):
        return "User({}, {})".format(self.username, self.password)

class Interpretation_session(db.Model, UserMixin):

    __tablename__ = "interpretation_session"

    id = db.Column(db.Integer, primary_key=True)
    startdate = db.Column(DateTime)
    enddate = db.Column(DateTime)
    session_comment = db.Column(db.Text)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    patient_ID = db.Column(db.String(10), db.ForeignKey('patients.patient_ID')) # Foreign
    session = db.relationship('Interpretations', backref=db.backref('session'))
    sample_id = db.Column(db.Integer, db.ForeignKey('samples.id'))
    comment = db.Column(db.Text)
    

    def __repr__(self):
        return "Interpretation session({}, {}, {})".format(self.patient_ID, self.startdate, self.enddate)


# variants2interpretation_session = db.Table('variants2interpretation_session',
#     db.Column('variant_id', db.Integer, db.ForeignKey('variants.id'), primary_key=True),
#     db.Column('interpretation_session_id', db.Integer, db.ForeignKey('interpretation_session.id'), primary_key=True))


class Patients(db.Model, UserMixin):

    __tablename__ = "patients"

    patient_ID = db.Column(db.String(10), primary_key=True, nullable=False)
    clinical_info = db.Column(db.Text)
    family_ID = db.Column(db.Integer)
    sex = db.Column(db.String(1))
    disease_category = db.Column(db.Text)
    CheckConstraint('sex IN ("M", "F"))') # Obs, denne fungerer ikke...
    samples = db.relationship('Samples', backref='samples', lazy=True)
    interpretation_sessions = db.relationship('Interpretation_session', backref="patient", lazy=True)


    def __repr__(self):
        return "Patients({}, {}, {}, {}, {})".format(self.patient_ID, self.clinical_info, self.family_ID, self.sex , self.disease_category)

class Samples(db.Model, UserMixin):

    __tablename__ = "samples"

    id = db.Column(db.Integer, primary_key=True)
    panel = db.Column(db.String(10))
    genelist = db.Column(db.String(255))
    mean_target_cov = db.Column(db.Integer)
    X20 = db.Column(db.Float)
    X30 = db.Column(db.Float)
    insert_size = db.Column(db.Integer)
    ti_tv = db.Column(db.Float)
    x_variants = db.Column(db.Integer)
    het_hom_x = db.Column(db.Float)
    total_snps = db.Column(db.Integer)
    predicted_sex = db.Column(db.String(7))
    bwa = db.Column(db.String(55))
    picard = db.Column(db.String(55))
    gatk = db.Column(db.String(55))
    fastqc = db.Column(db.String(55))
    mosdepth = db.Column(db.String(55))
    snpEff = db.Column(db.String(55))
    snpSift = db.Column(db.String(55))
    vcfanno = db.Column(db.String(55))
    nextflow = db.Column(db.String(55))
    hg = db.Column(db.String(8), default="hg19")
    patient_ID = db.Column(db.String(10), db.ForeignKey('patients.patient_ID'))
    run_ID = db.Column(db.Integer, db.ForeignKey('runs.id'))
    interpretations = db.relationship('Interpretation_session', backref="samples", lazy=True) # This is for the seessions


    def __repr__(self):
        return "Samples({}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {})".format(self.patient_ID,
                                                self.panel, self.genelist, self.mean_target_cov, self.X20, self.X30, self.insert_size,
                                                self.ti_tv, self.x_variants, self.het_hom_x, self.total_snps,
                                                self.predicted_sex, self.bwa,
                                                self.picard, self.gatk,
                                                self.fastqc, self.mosdepth,
                                                self.snpEff, self.snpSift,
                                                self.vcfanno, self.nextflow)

class Runs(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    sbs = db.Column(db.String(52), unique=True)
    fcid = db.Column(db.String(12))
    numCycles = db.Column(db.Integer)
    clusterDensity = db.Column(db.Float)
    clustersPassingFilter = db.Column(db.Float)
    estimatedYield = db.Column(db.Float)
    date = db.Column(DateTime, default=datetime.datetime.utcnow)
    samples = db.relationship('Samples', backref="run")

    def __repr__(self):
        return "Runs({}, {}, {}, {})".format(self.id, self.sbs, self.date, self.samples)

class Variants(db.Model, UserMixin):

    __tablename__ = "variants"

    id = db.Column(db.Integer, primary_key=True)
    chr = db.Column(db.String(5))
    pos = db.Column(db.Integer)
    ref = db.Column(db.String(50))
    alt_allele = db.Column(db.String(50))
    annotations = db.relationship('Annotations', backref=db.backref('variants'))
    #interpretation_session = db.relationship('Interpretation_session', secondary=variants2interpretation_session, backref=db.backref('variants', lazy='dynamic')) # This is for the sessions
    interpretation = db.relationship('Interpretations', backref=db.backref('variants'))

    __table_args__ = (db.UniqueConstraint(chr, ref, pos, alt_allele),)

    def __repr__(self):
        return "Variants(chr:{} pos:{} ref:{} alt_allele:{})".format(self.chr, self.pos, self.ref, self.alt_allele )

class Annotations(db.Model, UserMixin):
    ''' Should be updated / inserted only if there is a change in the annotation. In that case a new ID should be created.'''
    __tablename__ = "annotations"

    id = db.Column(db.Integer, primary_key=True)
    variant_id = db.Column(db.Integer, db.ForeignKey('variants.id'))
    ExAC_AF_NFE = db.Column(db.REAL)
    ExAC_AC_NFE = db.Column(db.Integer)
    ExAC_Hom_NFE = db.Column(db.Integer)
    ExAC_AN_NFE = db.Column(db.Integer)
    ExAC_AN = db.Column(db.Integer)
    ExAC_AC = db.Column(db.Integer)
    ExAC_Hom = db.Column(db.Integer)
    ExAC_AF = db.Column(db.REAL)
    ANN = db.Column(db.Text)


    def __repr__(self):
        return "id:{}, variant_id:{}, ExAC_AF_NFE: {}".format(self.id, self.variant_id, self.ExAC_AF_NFE)

class Interpretations(db.Model, UserMixin):

    __tablename__ = "interpretations"

    id = db.Column(db.Integer, primary_key=True)
    interp_id = db.Column(db.Integer, db.ForeignKey('interpretation_session.id'))
    variant_id = db.Column(db.Integer, db.ForeignKey('variants.id'))
    QUAL = db.Column(db.Integer)
    FILTER = db.Column(db.Text)
    AC = db.Column(db.Integer)
    AF = db.Column(db.Float)
    AN = db.Column(db.Integer)
    AD = db.Column(db.Text)
    GQ = db.Column(db.Integer)
    DPth = db.Column(db.Integer)
    PL = db.Column(db.Text)
    GT = db.Column(db.String(4))
    comment = db.Column(db.Text)
    variant_class = db.Column(db.Integer)


    def __repr__(self):
        return "Interpretations(id: {}, inter_id: {})".format(self.id, self.interp_id)
