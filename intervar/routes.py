from flask import g, render_template, request, flash, redirect, url_for, abort, jsonify
from flask_login import LoginManager, login_user, logout_user, login_required, current_user
from intervar.forms import Interpretation, Interpretations_form
from intervar.models import User, Patients, Samples, Runs, Annotations, Interpretations, Variants, Interpretation_session
from intervar.codeBase import querySamplesForInt, merge_three_dicts
from intervar import app, db
from datetime import datetime
import json

################################################################################################################################################
# Boilerplate:
# user database - login - index-page
################################################################################################################################################
# Debug mode makes auto reload on save
#DEBUG = True
#WTF_CSRF_ENABLED = True
#SECRET_KEY = 'yekterces'
#SQLALCHEMY_DATABASE_URI = 'sqlite:///db/intervar.db'

# manage logins and users
login_manager = LoginManager(app)
login_manager.init_app(app)
login_manager.login_view = 'login'

def get_db():
    db = getattr(g, '_database', None)
    if db is None:
        db = g._database = connect_to_database()
    return db

@app.teardown_appcontext
def close_connection(exception):
    db = getattr(g, '_database', None)
    if db is not None:
        db.close()

@login_manager.user_loader
def user_loader(user_id):
    user = User.query.filter_by(id=user_id)
    if user.count() == 1:
        return user.one()
    return None

@app.before_first_request
def init_request():
    db.create_all()

@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('index'))

@app.route('/register', methods=['GET', 'POST'])
def register():
    if request.method == 'GET':
        return render_template('register.html')
    elif request.method == 'POST':
        username = request.form['txtUsername']
        password = request.form['txtPassword']
        user = User.query.filter_by(username=username)
        if user.count() == 0:
            user = User(username=username, password=password)
            db.session.add(user)
            db.session.commit()
            flash('You have registered the username {0}. Please login'.format(username))
            return redirect(url_for('login'))
        else:
            flash('The username {0} is already in use.  Please try a new username.'.format(username))
            return redirect(url_for('register'))
    else:
        abort(405)


################################################################################################################################################
# Login module:
################################################################################################################################################
@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'GET':
        return render_template('login.html', next=request.args.get('next'))
    elif request.method == 'POST':
        username = request.form['txtUsername']
        password = request.form['txtPassword']

        user = User.query.filter_by(username=username).filter_by(password=password)
        if user.count() == 1:
            login_user(user.one())
            flash('Welcome back {0}'.format(username))
            try:
                next = request.form['next']
                return redirect(next)
            except:
                return redirect(url_for('index'))
        else:
            flash('Invalid login')
            return redirect(url_for('login'))
    else:
        return abort(405)


################################################################################################################################################
# index page:
################################################################################################################################################
@app.route('/')
def index():
    return render_template('index.html')





################################################################################################################################################
# Site for selecting samples that are run:
################################################################################################################################################
@app.route('/interpretations', methods=['GET', 'POST'])
@login_required
def interpretations():
    form = Interpretations_form()
    # Select samples that are ready for interpretation:
    samples_for_interpret = querySamplesForInt()
    if request.method == "GET":


        return render_template('interpretations.html', form=form, terplist=samples_for_interpret)
    elif request.method == 'POST':
        pID = request.form['pID']
        if len(Interpretations.query.join(Interpretation_session).filter(Interpretation_session.patient_ID == "1161_19").all()) == 0:
            flash('No variants here, go straigt to report!!')
            return redirect(url_for('interpret', pID=pID))
        else:
            return redirect(url_for('interpret', pID=pID))

    else:
        abort(405)




################################################################################################################################################
# Interpret page:
################################################################################################################################################
@app.route('/interpret/', methods=['GET', 'POST'])
@app.route('/interpret/<pID>', methods=['GET', 'POST'])
@login_required
def interpret(pID):

    form = Interpretation()
    # Query alle varianter for en pasient:
    variantsDict = {}
    for record in db.session.query(Variants, Interpretations, Interpretation_session, Annotations)\
        .join(Annotations)\
        .join(Interpretations, Interpretations.variant_id == Variants.id)\
        .join(Interpretation_session)\
        .filter(Interpretation_session.patient_ID == pID)\
        .distinct()\
        .all():
        variantsDict[record.Variants.id] = merge_three_dicts(record.Interpretations.__dict__, record.Variants.__dict__, record.Annotations.__dict__)


    # for key, value in variantsDict.items():
    #     for k, v in value.items():
    #         print "{}-{}".format(k,v)



    if request.method == 'POST':
        print "hell yeah"
        if form.validate_on_submit() and request.form['submit'] == "Save interpretation":
            # If startdate earlier than today, do not insert startdate, only enddate
            jsdata =  json.loads(request.form['jsdata'])

            # User
            print current_user.username
            
            sess_id = Interpretation_session.query.filter_by(patient_ID = pID).first()
            sess_id.enddate = datetime.now()
            sess_id.comment = request.form['comments']
            sess_id.patient = Patients.query.filter_by( patient_ID = pID).first()
            db.session.add(sess_id)
            db.session.commit()
            # Update variant comment and class where present

            # Iterate the dict and update if present.
            
            for k, v in jsdata.items():
                if not v["comment"] is None:
                    print "----: {} :------".format(v["comment"].encode("utf-8"),)
                    # Sjekk om entry allerede eksisterer.
                    print "Id: {}".format(db.session.query(Interpretations).filter(Interpretations.id == v["interp_id"]).first().id   )
                else:
                    print "heeeer er jeg"
                    #int_id = Interpretations.query.filter(Interpretations.id == v["interp_id"]).first()
                    #int_id = db.session.query(Interpretations).filter(Interpretations.id == v["interp_id"]).first()

                    #if int_id.comment:
                    #    print "not empty"
                    #    print v
                    #else:
                       # print "empty"
                    #int_id.comment = v["comment"]
                    #int_id.variant_class = v["variant_class"]
                    #db.session.add(int_id)
                if not v["variant_class"] is None:
                    print "----: {} :------".format(v["variant_class"],)


            db.session.commit()

            return redirect(url_for('interpretations', pID=pID))
            #return render_template('interpret.html', form=form, pID=pID, variantsDict=variantsDict)


            #return render_template('interpret.html', form=form, pID=pID, variantsDict=variantsDict, user=current_user.username)
            #return render_template('interpretations.html', form=form, terplist=samples_for_interpret)
            flash('Varianten er lagret, tolke en til??')
            return redirect(url_for('interpretations', pID=pID))



    elif request.method == "GET":
        print current_user.username
        sess_id = Interpretation_session.query.filter_by(patient_ID=pID).first()
        if db.session.query(Interpretation_session).filter(Interpretation_session.startdate != None).first():
            sess_id.startdate = datetime.now()
            sess_id.user = User.query.filter_by(username=current_user.username).first()
            db.session.add(sess_id)
            db.session.commit()
        return render_template('interpret.html', form=form,  pID=pID, variantsDict=variantsDict)
    else:
        abort(405)


################################################################################################################################################
# API for groovy reportwriter:
# Groovy script gets json as response from this route
################################################################################################################################################
@app.route('/api', methods=['GET'])
@app.route('/api/<pID>', methods=['GET'])
def api(pID):

    return jsonify(username="g.user.username", email="g.user.email", id="g.user.id")
