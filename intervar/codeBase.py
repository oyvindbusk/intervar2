from intervar import db
from intervar.models import User, Patients, Samples, Runs, Interpretation_session, Variants, Annotations, Interpretations
import pandas as pd
import io
import re
import json
from datetime import datetime
from sqlalchemy import Integer, ForeignKey, CheckConstraint
from sqlalchemy import exc

def querySamplesForInt():
    '''
    Function to query the db and get back samples ready for interpretation, and samples where interpretation is begun, but not completed.

    '''
    # Hent ut alle Interpretation_sessions uten dato:
    samples = Interpretation_session.query.filter(Interpretation_session.enddate == None).all()
    print "samples: {}".format(samples)
    terplist = []
    for sample in samples:
        print sample
        terplist.append( {"sID" : sample.patient.patient_ID,
        "panel": Samples.query.filter_by(patient_ID=sample.patient.patient_ID).first().panel,
        "user": sample.user_id})

    return terplist


class Vcf:
    ''' A Class with methods for getting variants from vcf-file and making ready for input into the db'''
    def __init__(self, path):
        self.path = path
        self.vcf = self.read_vcf()
        self.sample_name = self.vcf.columns[9]
        self.df = self.df_expand()
        self.filtered_df = self.return_filtered_variants_with_annotations()

    def printcols(self):
        print "A total of {} columns.".format(len(self.cols))
        for col in self.cols:
            print "Column name: {}".format(col)

    def read_vcf(self):
        with open(self.path, 'r') as f:
            lines = [l for l in f if not l.startswith('##')]
        return pd.read_csv(io.StringIO(u''.join(lines)), dtype={'#CHROM': str, 'POS': int, 'ID': str, 'REF': str, 'ALT': str,'QUAL': str, 'FILTER': str, 'FORMAT': str, 'INFO': str},\
        sep='\t').rename(columns={'#CHROM': 'chr', 'POS': 'pos', 'REF':'ref', 'ALT':'alt_allele'})

    def df_expand(self):
        ''' Expand the INFO-field and FORMAT into separate cols, return a new DF without INFO and with the expanded fields'''

        df = pd.DataFrame(x for x in self.vcf['INFO'].apply(self.split_INFO))
        df = pd.concat([self.vcf.drop(['INFO'], axis=1), df], axis=1)
        df = self.split_FORMAT(df)
        df.drop(['FORMAT', self.sample_name], axis = 1, inplace = True)
        return df

    def split_INFO(self, string):
        matches = re.findall(r'\w+=".+?"', string) + re.findall(r'\w+=[&\*/+,>\d\w.\|-]+', string)
        matches = [m.split('=') for m in matches]
        d = dict(matches)
        return d


    def split_FORMAT(self, df):
        sample_name = self.sample_name

        #df = pd.DataFrame(df.loc[:, sample_name].str.split(':',-1).tolist(),columns = ['GT','AD','DPth','HETHOM','GQ','PL','1','2','3']).drop(['1','2','3'], axis=1)
        df_PGT = df[df['FORMAT'].str.contains("PID")]
        df_PGT_exp = df_PGT.loc[:, sample_name].str.split(':',-1, expand=True)
        df_PGT_exp.rename(columns = {0:'GT', 1:'AD', 2:'DPth',3:'HETHOM',4:'GQ',5:'PGT',6:'PID',7:'PL',8:'PS'}, inplace = True)

        df_PGT = pd.concat([df_PGT, df_PGT_exp], axis = 1)
        df_PGT.drop(['PGT', 'PID', 'PS'], axis = 1, inplace = True)

        # get rows not containing PID, split them, add headers and remove the splitted field
        df_nonPGT = df[~df['FORMAT'].str.contains("PID")].loc[:,:]
        df_nonPGT_exp = df_nonPGT.loc[:, sample_name].str.split(':',-1, expand=True)
        df_nonPGT_exp.rename(columns = {0:'GT', 1:'AD', 2:'DPth',3:'HETHOM',4:'GQ',5:'PL'}, inplace = True)
        df_nonPGT = pd.concat([df_nonPGT, df_nonPGT_exp], axis = 1)

        # Concat the two separate tables back into one
        df = pd.concat([df_nonPGT, df_PGT])
        return df


    def return_variants_df(self):
        ''' Returns a list of dictionarys containing input to the Variants-table '''
        cols_to_select = ['chr','pos','ref','alt_allele','GT']
        df = self.df
        return df.loc[:,cols_to_select].to_dict(orient="records")


    def return_filtered_variants_with_annotations(self):
        cols_to_select = ['chr','pos','ID','ref','alt_allele','QUAL','FILTER','AC','AF','AN','AD','PL','GT', 'GQ', 'DPth','ExAC_AF_NFE', 'ExAC_AC_NFE', 'ExAC_Hom_NFE', 'ExAC_AN_NFE', 'ExAC_AN', 'ExAC_AC', 'ExAC_Hom', 'ExAC_AF', 'ANN']

        df = self.df

        from intervar.filters import ad, skip_scientific
        df = skip_scientific(df)
        df = ad(df)

        filtered_df = df.loc[:,cols_to_select].to_dict(orient="records")

        return filtered_df







def insert_sample_vcf(samplejson, vcf):
    print "start"
    f = open(samplejson, 'r')
    json_content = json.load(f)
    sample_id = json_content["information"]["sample"]["id"]
    print sample_id
    print "**inserting sample file into patients database**"
    db.session.add(Patients(patient_ID = json_content["information"]["sample"]["id"],
        clinical_info = "Mock clinical info",
        family_ID = 120,
        sex = "{sex}".format(sex = "M" if json_content["information"]["sample"]["qc"]["Predicted sex"] == "male" else "F"),
        disease_category = "Nevro"
        ))
    db.session.commit()
    print "**inserted sample file into patients database for sample: {}**".format(sample_id)
    

    print "**inserted samplefile entries into samples database**"
    if db.session.query(Runs).filter(Runs.sbs == json_content["information"]["run"]["id"]).first():
        pass
    else:
        db.session.add(Runs(
            sbs = json_content["information"]["run"]["id"],
            fcid = json_content["information"]["run"]["fcid"],
            numCycles  = json_content["information"]["run"]["numCycles"],
            clusterDensity= json_content["information"]["run"]["clusterDensity"],
            clustersPassingFilter= json_content["information"]["run"]["clustersPassingFilter"],
            estimatedYield = json_content["information"]["run"]["estimatedYield"],
            date = datetime.strptime(json_content["information"]["run"]["date"], '%Y%m%d')
            ))
        db.session.commit()

        
    db.session.add(Samples(
        panel = json_content["information"]["sample"]["panel"],
        genelist  = json_content["information"]["sample"]["filtexpath"],
        mean_target_cov  = json_content["information"]["sample"]["qc"]["mean coverage"],
        X20  = json_content["information"]["sample"]["qc"]["% > 20 X"],
        X30  = json_content["information"]["sample"]["qc"]["% > 30 X"],
        insert_size  = json_content["information"]["sample"]["qc"]["insert size"],
        ti_tv  = json_content["information"]["sample"]["qc"]["ti/tv"],
        x_variants  = json_content["information"]["sample"]["qc"]["Variants on X"],
        het_hom_x  = json_content["information"]["sample"]["qc"]["X het/hom-ratio"],
        total_snps  = json_content["information"]["sample"]["qc"]["total snps"],
        predicted_sex = json_content["information"]["sample"]["qc"]["Predicted sex"],
        bwa = json_content["information"]["pipeline"]["software versions"]["bwa"],
        picard = json_content["information"]["pipeline"]["software versions"]["picard"],
        gatk = json_content["information"]["pipeline"]["software versions"]["gatk"],
        fastqc = json_content["information"]["pipeline"]["software versions"]["fastqc"],
        mosdepth = json_content["information"]["pipeline"]["software versions"]["mosdepth"],
        snpEff = json_content["information"]["pipeline"]["software versions"]["snpEff"],
        snpSift = json_content["information"]["pipeline"]["software versions"]["snpSift"],
        vcfanno = json_content["information"]["pipeline"]["software versions"]["vcfanno"],
        nextflow = json_content["information"]["pipeline"]["software versions"]["nextflow"],
        patient_ID = json_content["information"]["sample"]["id"],
        run_ID = db.session.query(Runs).filter(Runs.sbs == json_content["information"]["run"]["id"]).with_entities(Runs.id).first()[0]
        ))
    db.session.commit()

    f.close()
    print "**inserted runs into runs database if not exists**"

    
    v = Vcf(vcf)
    variants = v.filtered_df
    # LAg en interp_session
    i_sess = Interpretation_session(
        patient=Patients.query.filter_by(patient_ID = sample_id).first()
    )
    db.session.add(i_sess)
    db.session.commit()
    
    for variant in variants:
        
        # Select the the columns used in the variants-table
        keys = ['chr', 'pos', 'ref', 'alt_allele']
        variants_only = dict((k, variant[k]) for k in keys if k in variant)
        
        # Select the the columns used in the annotations-table
        annot_keys = ['ExAC_AF_NFE', 'ExAC_AC_NFE', 'ExAC_Hom_NFE', 'ExAC_AN_NFE', 'ExAC_AN', 'ExAC_AC', 'ExAC_Hom', 'ExAC_AF', 'ANN']
        annot_variants  = dict((k, variant[k]) for k in annot_keys if k in variant)
        annot = Annotations(**annot_variants)
        
        # Select the the columns used in the interpretation-table
        interp_keys = ['QUAL','FILTER','AC','AF','AN','AD','PL','GT', 'GQ', 'DPth']
        interp_variants = dict((k, variant[k]) for k in interp_keys if k in variant)

        
        # Add the variant
        f = Variants( **variants_only)
        try:
                        
            # Add the Interpretations via the backref interpretation in the Variants-class:
            interp = Interpretations(**interp_variants)            
            f.interpretation.append(interp)
                        
            # Add the interp to a session via backref session in Interpretation_session class
            i_sess.session.append(interp)
            db.session.add(interp)
            
            
            # Add the annotations
            f.annotations.append(annot)
                     
            # Commit:
            db.session.commit()
        except exc.SQLAlchemyError as e:
            
            print "YEssireeee "
            # I have added one of the variants from 1161 APN to 1302 Ataksi, this should end up here because of unique constraint on the variants-table
            db.session.rollback()
            # So.. I need to query the db for a variant:
            var = Variants.query.filter(Variants.chr == variants_only['chr'], Variants.pos == variants_only['pos'], Variants.ref == variants_only['ref'], Variants.alt_allele == variants_only['alt_allele']).first()
            
            
            # Add the Interpretations via the backref interpretation in the Variants-class:
            interp = Interpretations(**interp_variants)            
            var.interpretation.append(interp)
            
            # Add the interp to a session via backref session in Interpretation_session class
            i_sess.session.append(interp)
            db.session.add(interp)

            # commitorama
            db.session.commit()
            
        
    db.session.commit()
        

    
    


    print "stop"

def merge_three_dicts(w, x, y):
    ''' Function to merge three dicts from a sqlalchemy-query'''

    del w['_sa_instance_state']
    w['iid'] = w.pop('id')
    del x['_sa_instance_state']
    del y['_sa_instance_state']
    z = w.copy()   # start with x's keys and values
    z.update(x)    # modifies z with y's keys and values & returns None
    z.update(y)
    return z
