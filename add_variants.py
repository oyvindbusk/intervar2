from intervar import db
from intervar.models import User, Patients, Samples, Runs, Interpretation_session, Interpretations, Variants
from sqlalchemy import Integer, ForeignKey, CheckConstraint
from sqlalchemy import exc
import json
from datetime import datetime
# Get data from a vcf:
from intervar.codeBase import Vcf


v = Vcf('tests/input_data/1162_19.raw.variants.Filtered.individual.annotated.subset.HM.vcf')

variants = v.return_variants_df()
for variant in variants:
    # To avoid error if duplicates:
    db.session.add(Variants(**variant))
    try:
        db.session.commit()
    except exc.SQLAlchemyError as e:
        db.session.rollback()
