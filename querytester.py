from intervar import db
from intervar.models import User, Patients, Samples, Runs, Interpretation_session, Variants, Annotations, Interpretations



def querySamplesForInt():
    '''
    Function to query the db and get back samples ready for interpretation, and samples where interpretation is begun, but not completed.

    '''
    # Hent ut alle Interpretation_sessions uten dato:
    samples = Interpretation_session.query.filter(Interpretation_session.enddate == None).all()
    print "samples: {}".format(samples)
    terplist = []
    for sample in samples:
        print sample
        terplist.append( {"sID" : sample.patient.patient_ID,
        "panel": Samples.query.filter_by(patient_ID=sample.patient.patient_ID).first().panel,
        "user": sample.user_id})

    return terplist

print "Samples for intrepretations"
querySamplesForInt()


def queryAnyForInterp():
    ''' 
    query how many variants is present in a given sample
    
    
    '''
    #if len(db.session.query(Variants, Interpretations).join(Interpretations).filter(Interpretations.sample_id == pID).all()) == 0: 
    