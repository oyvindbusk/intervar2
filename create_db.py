from intervar import db
from intervar.models import User, Patients, Samples, Runs, Interpretation_session, Interpretations, Variants, Annotations
from sqlalchemy import Integer, ForeignKey, CheckConstraint
from sqlalchemy import exc
from datetime import datetime


from intervar.codeBase import insert_sample_vcf

db.drop_all()
db.create_all()

# First - add some users:
db.session.add(User(username='buso', password='buso'))
db.session.add(User(username='ohol', password='ohol'))
db.session.commit()


# Get data from a vcf:

insert_sample_vcf('tests/input_data/1273_19.sample', 'tests/input_data/1273_19.raw.variants.Filtered.individual.annotated.subset.HSP.vcf')
insert_sample_vcf('tests/input_data/1302_19.sample', 'tests/input_data/1302_19.raw.variants.Filtered.individual.annotated.subset.Ataksi.vcf')
insert_sample_vcf('tests/input_data/1162_19.sample', 'tests/input_data/1162_19.raw.variants.Filtered.individual.annotated.subset.HM.vcf')
insert_sample_vcf('tests/input_data/1161_19.sample', 'tests/input_data/1161_19.raw.variants.Filtered.individual.annotated.subset.APN.vcf')


