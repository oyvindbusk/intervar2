FROM centos:centos7
MAINTAINER Øyvind "oyvindbusk@gmail.com"
RUN yum -y update
RUN yum install -y epel-release
RUN yum install -y python-pip python-dev build-essential
COPY ./requirements.txt /app/requirements.txt
WORKDIR /app
RUN pip install --upgrade pip
RUN pip install -r requirements.txt
CMD [ "python", "/intervar/run.py" ]
